import os
import numpy as np
import json


def split_dataset(test_ratio: float):
    if not(test_ratio > 0.0 and test_ratio < 1.0):
        raise ValueError('Test ratio must be greater than 0 and less than 1')

    annotations_location = os.path.join(os.getcwd(), 'annotations')
    test_file = open(os.path.join(annotations_location, 'test.csv'), 'w')
    train_file = open(os.path.join(annotations_location, 'train.csv'), 'w')
    data = open(os.path.join(annotations_location, 'data.json')).readlines()

    np.random.seed()
    data_permutations = np.random.permutation(len(data))

    split_length = int(len(data) * test_ratio)
    test_data = data_permutations[:split_length]
    train_data = data_permutations[split_length:]

    write_csv_file(test_data, data, test_file)
    print('Created test annotation file')

    write_csv_file(train_data, data, train_file)
    print('Created train annotation file')


def write_csv_file(data, all_data, file):
    for line_number in data:
        line = json.loads(all_data[line_number].strip())
        write_line_to_csv(line, file)
    file.close()


def write_line_to_csv(line, file):
    joints = sorted([[int(key), value] for key, value in line['joint_pos'].items()])
    joints = np.array([j for i, j in joints]).flatten()

    csv_data = [line['filename']]
    csv_data.extend(joints)
    
    file.write(','.join([str(field) for field in csv_data]) + '\n')


if __name__ == "__main__":
    split_dataset(0.1)