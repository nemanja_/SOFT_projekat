import argparse


def read_single_person_list():
    f = open("single_person_image_ids.txt", "r")
    image_id_list = f.readlines()
    f.close()
    return image_id_list


def create_arg_parser():
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument('-out_path', type=str)
    argument_parser.add_argument('-test_path', type=str)
    return argument_parser


if __name__ == "__main__":
    parser = create_arg_parser()
    args = parser.parse_args()
    image_ids = read_single_person_list()
    out_file = open(args.out_path, 'w')
    for line in open(args.test_path, 'r'):
        for one in image_ids:
            if one in line:
                out_file.write(line)
    out_file.close()